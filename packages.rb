r = Repository::current_repository
r.name= "auksys"
r.add_depend Git, "https://gitlab.com/cyloncore/lrs-pkg-core-repositories.git", push_url: "git@gitlab.com:cyloncore/lrs-pkg-core-repositories.git"

# Knowledge Database
conformance_sparql_pkg = create_package('kDB-SPARQL-Conformance', 'Knowledge Database', 'Knowledge',
                                        [], [], 'https://gitlab.com/auksys/kdb-sparql-conformance.git', Git, Recipe)
  .add_option('vc.push_url', 'git@gitlab.com:auksys/kdb-sparql-conformance.git')
conformance_shacl_pkg = create_package('w3c-data-shapes', 'data-shapes', 'Knowledge', [], [], 'https://github.com/w3c/data-shapes.git', Git, Recipe)
create_package('knowL', 'Knowledge Libraries and Explorer', 'Knowledge',
               [ 'cext'], ['Cartography', 'ag', 'w3c-data-shapes', 'pybind11-qt'],
               'https://gitlab.com/auksys/knowL.git', Git, Recipes::LrsRecipe)
  .add_option("recipe.cmake.KNOWL_W3C_DATA_SHAPES_DIR", conformance_shacl_pkg.checkout_directory)
  .add_option('vc.push_url', 'git@gitlab.com:auksys/knowL.git')

create_package('kDB', 'Knowledge Database', 'Knowledge',
               ['parc', 'Cyqlops', 'knowL', 'ag'], ['kDB-SPARQL-Conformance'],
               'https://gitlab.com/auksys/kdb.git', Git, Recipes::LrsRecipe)
  .add_option("recipe.cmake.KDB_SPARQL_CONFORMANCE_DIR", conformance_sparql_pkg.checkout_directory)
  .add_option('vc.push_url', 'git@gitlab.com:auksys/kdb.git')

# scQL
create_package('scQL', 'SymbiCloud Query Language', 'Knowledge', ['knowL'], ['kDB', 'TstML'], 'git@gitlab.com:auksys/scql.git', Git, Recipes::LrsRecipe)

  
# Mission and Knowledge Center
  
create_package('mkc_clients', 'Mission and Knowledge Center Clients', 'Knowledge',
               [], [],
               'git@gitlab.com:auksys/mkc_clients.git', Git, Recipes::LrsRecipe)
create_package('mkc_server', 'Mission and Knowledge Center Server', 'Knowledge',
               ['kDB', 'TstML', 'scQL'], [],
               'git@gitlab.com:auksys/mkc_server.git', Git, Recipes::LrsRecipe)

# Multi-agent

create_package('TstML', 'TST manipulation library for C++ and QML', 'Robotics', ['ptc'], ['rosqml'], "https://gitlab.com/auksys/tstml.git", Git, Recipes::LrsRecipe)
  .add_option('vc.push_url', 'git@gitlab.com:auksys/tstml.git').add_option('vc.switch_branch', ['master', 'stable'])
create_package('agent-tk', 'Agent Framework.', 'auksys', [], [], 'git@gitlab.com:auksys/agent-tk.git', Git, Recipes::CargoRecipe)
create_package('matks', 'Multi-Agent Task Simulator.', 'auksys', [], [], 'git@gitlab.com:auksys/matks.git', Git, Recipes::CargoRecipe)

# pralin
create_package('libnabo', 'A fast K Nearest Neighbor library for low-dimensional spaces', '', [], [], "git@github.com:ethz-asl/libnabo.git", Git, Recipes::LrsRecipe)
create_package('libpointmatcher', 'Fast ICP implementation', 'EnvironmentModeling', ['libnabo'], [], "git@github.com:ethz-asl/libpointmatcher.git", Git, Recipes::LrsRecipe)
create_package('llama.cpp', 'Inference of Meta\'s LLaMA model (and others) in pure C/C++', 'NLP', [], [], 'https://github.com/ggerganov/llama.cpp.git', Git, Recipes::CmakeRecipe)
create_package('llama-xpp', 'High level C++ interface to LLama', 'NLP', ['llama.cpp'], [], 'https://gitlab.com/auksys/llama-xpp.git', Git, Recipes::CmakeRecipe).add_option('vc.push_url', 'git@gitlab.com:auksys/llama-xpp.git')
create_package('pralin', 'PRocessing ALgorithms INterface', 'Misc', ['cext'], ['tasks_machine', 'llama-xpp'], 'https://gitlab.com/auksys/pralin.git', Git, Recipes::CmakeRecipe).add_option('vc.push_url', 'git@gitlab.com:auksys/pralin.git').add_env_defininiton('PRALIN_TEST_DATA_DIR', "#{ENV['LRS_PKG_ROOT']}/src/pralin/data")

# Rust
create_package('kDB_rs', 'kDB Rust Integration.', 'auksys', [], [], 'git@gitlab.com:auksys/kdb_rs.git', Git, Recipes::CargoRecipe)
create_package('auksys', 'Tool for easilly running `auksys` containers.', 'auksys', [], [], 'git@gitlab.com:auksys/auksys.git', Git, Recipes::CargoRecipe)
create_package('kproc', 'Knowledge Processing.', 'auksys', [], [], 'git@gitlab.com:auksys/kproc.git', Git, Recipes::CargoRecipe).add_option("recipe.disable_install", true)
create_package('paia', 'Persistent Artifcial Intelligent Agent.', 'auksys', [], [], 'git@gitlab.com:auksys/paia.git', Git, Recipes::CargoRecipe).add_option('recipe.install_paths', ["crates/paia", "crates/paia-ui"])
